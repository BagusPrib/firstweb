from django import forms

from .models import schedule

class ScheduleForm(forms.Form):
    attrsText = {
        'class' : 'form-control'
    }
    
    attrsDate = {
        'class' : 'form-control',
        'type' : 'date'
    }
    
    attrsTime = {
        'class' : 'form-control',
        'type' : 'time'
    }

    date = forms.DateField(label = "Date", widget=forms.DateInput(attrs=attrsDate), required = True)
    time = forms.TimeField(label = "Time", widget=forms.TimeInput(attrs=attrsTime), required = True)
    name = forms.CharField(label = "Activity", max_length=30, widget=forms.TextInput(attrs=attrsText), required = True)
    place = forms.CharField(label = "Location", max_length=30, widget=forms.TextInput(attrs=attrsText),required = True)
    type = forms.CharField(label = "Type", max_length=30, widget=forms.TextInput(attrs=attrsText), required = True)