from django.db import models
from django.utils import timezone

class schedule(models.Model):
    date = models.DateField(max_length = 30, default = timezone.now)
    time = models.TimeField(max_length = 30, default = timezone.now)
    name = models.CharField(max_length = 30)
    place = models.CharField(max_length = 30)
    type = models.CharField(max_length = 30)