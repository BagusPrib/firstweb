from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.shortcuts import redirect
from .forms import ScheduleForm
from .models import schedule

response = {'author' : 'Bagus Pribadi'}
def aboutMe(request):
    return render(request, 'aboutMe.html')
    
def message(request) :
    return render(request, 'message.html')
    
def relations(request) :
    return render(request, 'relations.html')
    
def clear(request):
    sch = schedule.objects.all()
    sch.delete()
    form = ScheduleForm()
    context = {
        'form' : form,
        'sch' : sch
    }
    return render(request, 'Schedule.html', context)
    
def Schedule(request):
    sch = schedule.objects.all()
    form = ScheduleForm(request.POST)
    if request.method == "POST":
        response['date'] = request.POST['date']
        response['time'] = request.POST['time']
        response['name'] = request.POST['name']
        response['place'] = request.POST['place']
        response['type'] = request.POST['type']
        schs = schedule(date = response['date'], time = response['time'], name = response['name'], place = response['place'], type = response['type'])
        schs.save()
        form = ScheduleForm()
        context = {
            'form' : form,
            'sch' : sch
        }
        return render(request, 'Schedule.html', context)
    else:
        form = ScheduleForm()
        context = {
            'form' : form,
            'sch' : sch
        }
        return render(request, 'Schedule.html', context)
