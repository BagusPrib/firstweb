from django.conf.urls import url
from .views import aboutMe, message, relations, Schedule

#url for app
urlpatterns = [
    url('aboutMe',aboutMe),
    url('message',message),
    url('relations',relations),
    url('Schedule',Schedule),
    url('',aboutMe)
]